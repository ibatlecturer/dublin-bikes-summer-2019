export interface Station {
    number: number;
    name: string;
    position: Position;
}

export interface Position {

    lat: number;
    lng: number;
}
