import { Component, OnInit } from '@angular/core';
import { StationService } from '../_service/station.service';
import { Station } from '../_models/station';

@Component({
  selector: 'app-station',
  templateUrl: './station.component.html',
  styleUrls: ['./station.component.css']
})
export class StationComponent implements OnInit {

  constructor(private stationService: StationService) { }
  stations: Station[];
  ngOnInit() {

    this.getStations();
  }

  getStations(): void {

   this.stationService.getStations().subscribe(stations => {
     
    this.stations = stations;

    console.log(stations);

   });
  }


}
