import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StationService } from '../_service/station.service';
import { Station } from '../_models/station';

@Component({
  selector: 'app-stationdetail',
  templateUrl: './stationdetail.component.html',
  styleUrls: ['./stationdetail.component.css']
})
export class StationdetailComponent implements OnInit {

  constructor( private route: ActivatedRoute,
    private stationService: StationService) { }

  ngOnInit(): void {
    this.getStation();
  }

  station:Station;
  getStation(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.stationService.getStation(id)
      .subscribe(station => {
        this.station = station
        console.log(station);
      });
  }


}
