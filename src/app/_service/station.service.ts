import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Station } from '../_models/station';
import {Observable, of} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class StationService {
  
  private apiUrl = `https://api.jcdecaux.com/vls/v1/stations`;
  private apiKey = `013c96fda1ac6937698c8402e42b0c31f2cc081e`;
  private stationsUrl = `${this.apiUrl}?contract=dublin&apiKey=${this.apiKey}`;  // URL to web api
  constructor(  private http: HttpClient) { }

  getStations():  Observable<Station[]> {
     return this.http.get<Station[]>(this.stationsUrl)
   }

   getStation(id: number):  Observable<Station> {
    const stationUrl = `${this.apiUrl}/${id}?contract=dublin&apiKey=${this.apiKey}`
    return this.http.get<Station>(stationUrl)
  }
}
