import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StationComponent } from './station/station.component';
import { StationdetailComponent } from './stationdetail/stationdetail.component';


const routes: Routes = [
  {path: 'stations', component: StationComponent},
  {path: 'station/:id', component: StationdetailComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
